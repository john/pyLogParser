import sys;
import os;
import json
from lib import logFormat;
from lib import logOutputHandler as log;
from lib import argumentHandler as args;
from lib import lineReader;
from lib import geoIP;

class pyLogParser:
	def __init__(self):
		self.frozen = False;
		self.logging = log.LogOutputHandler();
		self.logFormat = logFormat.LogFormat();

		try:
			# try to parse command line arguments
			self.paremeters = args.ArgumentHandler();
		except FileNotFoundError as e:
			self.error('File not Found: ' + str(e), True);


		# determine if application is a script file or frozen exe
		if getattr(sys, 'frozen', False):
			self.frozen = True;
			application_path = os.path.dirname(sys.executable);
		elif __file__:
			application_path = os.path.dirname(__file__);

		# add path to binary to make import work when script is packed with pyinstaller
		sys.path.insert(0, application_path);
		try:
			os.chdir(application_path);
		except:
			pass;

		try:
			# try importing config file
			import configuration;
			# check that config object is accessible
			configuration.CONFIG;
		except Exception as e:
			error('configuration corrupt or missing!: ' + str(e), True)

		self.config = configuration.CONFIG;

		try:
			self.logging.setLogLevel(self.config.loglevel);
		except:
			pass;

		self.log_format = self.config['logFormat'];
		print_help = self.paremeters.get('PRINT_HELP');

		try:
			if self.paremeters.get('INPUT') is not False:
				self.input = lineReader.LineReader(self.paremeters.get('INPUT'));
			else:
				self.input = lineReader.LineReader();
		except Exception:
			self.error('cannot open input file: ' + self.paremeters.get('INPUT'), True);

		try:
			self.fieldList, self.regex = self.logFormat.parseFormatString(self.log_format);

		except Exception as e:
			self.error('the log format string ' + self.log_format + ' is invalid: ' + str(e), True);

		try:
			if self.frozen:
				self.geoip = geoIP.GeoIP(sys._MEIPASS + '/res/GeoLite2-Country.mmdb')
			else:
				self.geoip = geoIP.GeoIP('./res/GeoLite2-Country.mmdb');
		except Exception as e:
			self.geoip = False;
			self.logging.log('geoip2 module not found, geo location support will be disabled.: ' + str(e), log.LOG_WARNING);

		if self.geoip and ('CLIENT_IP' in self.fieldList):
			self.fieldList.append('COUNTRY');
		else:
			self.goeip = False;
			self.logging.log('geoip2 module found but clientip not in logformat, geo location support will be disabled.', log.LOG_WARNING);

		formatpath = False;
		if self.paremeters.get('FORMAT') is not False:
			formatpath = self.paremeters.get('FORMAT');
		elif 'format' in self.config:
			formatpath = self.config['format'];

		if formatpath:
			try:
				ffile = open(formatpath);
				self.output_def = json.loads(ffile.read());
			except ValueError as e:
				self.error('invalid format in format file: ' + formatpath + '  ' + e.msg, True);
			except Exception:
				self.error('cannot open format file: ' + formatpath, True);

		else:
			print_help = True;

		if print_help:
			print(self.paremeters.getHelp());
			sys.exit();

		self.processData();


	def error(self, message, critical = False):
		self.logging.log(message, log.LOG_CRITICAL)
		if critical:
			sys.exit();

	def die(self):
		error('die function was called', True);

	def split_line(self, line, regex):
		split = regex.match(line).groups();
		if self.geoip:
			split = split + (self.geoip.getCountryByIp(split[0]),);

		return split;


	def parse_line(self, def_node, line, target):

		if not (def_node['match_name'] in target):
			target[def_node['match_name']] = {};

		target = target[def_node['match_name']];

		if not (line[def_node['match_name']] in target):
			target[line[def_node['match_name']]] = {}

		t = target[line[def_node['match_name']]];

		if ('with_size' in def_node and def_node['with_size'] == True and ('RESPONSE_SIZE' in line)):
			if 'size' in t:
				t['size'] += int(line['RESPONSE_SIZE']);
			else:
				t['size'] = int(line['RESPONSE_SIZE']);

		if ('with_count' in def_node and def_node['with_count'] == True):
			if 'count' in t:
				t['count'] += 1;
			else:
				t['count'] = 1;

		if ('childs' in def_node):
			for c_node in range(len(def_node['childs'])):
				self.parse_line(def_node['childs'][c_node], line, t);

	def processData(self):
		try:
			o = dict();

			skipped_regex_fail = 0;
			skipped_format_fail = 0;

			for line in self.input.readLines():
				fields = self.split_line(line, self.regex);
				if not fields:
					skipped_regex_fail+=1;
					continue;

				data = {}
				if (len(self.fieldList) == len(fields)):
					for f in range(len(self.fieldList)):
						data[self.fieldList[f]] = fields[f];

					self.parse_line(self.output_def[0], data, o);
				else:
					skipped_format_fail+=1;

			output_json = json.dumps(o, ensure_ascii = True);
			if self.paremeters.get('OUTPUT') is not False:
				try:
					out = open(self.paremeters.get('OUTPUT'), 'w');
					out.write(output_json);
				except Exception as e:
					error('cannot write output to file, falling back to STDOUT: ' + str(e));
					print(output_json);
			else:
				print(output_json);
		except KeyboardInterrupt:
			print(" terminating");


parser = pyLogParser();
