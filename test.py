import sys

WARNING = 0;
NOTICE = 1;
DEBUG = 2;

verbose = 2;


def log(msg, log_level):
	if (log_level <= verbose):
		if log_level == WARNING:
			print('\033[93m' + 'Warning: ' + msg + '\033[0m');
		if log_level == NOTICE:
			print('\033[92m' + 'Notice: ' + msg + '\033[0m')
		if log_level == DEBUG:
			print('\033[94m' + 'Debug: ' + msg + '\033[0m')

def log_pass(descr):
	log(descr + ' : PASSED', NOTICE);
	return True;

def log_fail(descr):
	log(descr + ' : FAILED', WARNING);
	return False;

def test_in(val, list, descr):
	if val in list:
		return log_pass(descr);
	else:
		return log_fail(descr);

def test_true(val, descr):
	if val == True:
		return log_pass(descr);
	else:
		return log_fail(descr);

def test_false(val, descr):
	if val == False:
		return log_pass(descr);
	else:
		return log_fail(descr);

def test_equal(val1, val2, descr):
	if val1 == val2:
		return log_pass(descr);
	else:
		return log_fail(descr);

tests = [];

def test_argumentHandler():
	log('--- Testing argumentHandler class ---', NOTICE);

	from lib import argumentHandler;
	passed = True;

	p1 = argumentHandler.ArgumentHandler([]);
	passed &= test_equal(p1.get('INPUT'), 'STDIN', 'INPUT is STDIN, when -i / --input is not set');
	passed &= test_equal(p1.get('OUTPUT'), 'STDOUT', 'OUTPUT is STDOUT, when -o / --output is not set');
	passed &= test_false(p1.get('PRINT_HELP'), 'PRINT_HELP is false, when -h / --help is not set');

	p2 = argumentHandler.ArgumentHandler(['script_name_will_be_skipped','-h']);
	passed &= test_true(p2.get('PRINT_HELP'), 'PRINT_HELP is true, when -h is set');

	p3 = argumentHandler.ArgumentHandler(['script_name_will_be_skipped','--help']);
	passed &= test_true(p3.get('PRINT_HELP'), 'PRINT_HELP is true, when --help is set');

	p4 = argumentHandler.ArgumentHandler(['script_name_will_be_skipped','-i', '/path/to/file']);
	passed &= test_equal(p4.get('INPUT'), '/path/to/file', 'INPUT contains path, when -i and file path are set');

	p5 = argumentHandler.ArgumentHandler(['script_name_will_be_skipped','-i']);
	passed &= test_equal(p5.get('INPUT'), 'STDIN', 'INPUT contains STDIN, when -i is set and file path is not');


	return passed;
tests.append(test_argumentHandler);

def test_logFormat():
	log('--- Testing logFormat class ---', NOTICE);

	from lib import logFormat;
	passed = True;

	test_format = 'LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\"" combined';
	fieldList, regex = logFormat.LogFormat().parseFormatString(test_format);
	passed &= test_equal(fieldList, ['CLIENT_IP', 'CLIENT_IDENTIFIER', 'USERNAME', 'DATE', 'METHOD', 'PATH', 'HTTP_VERSION', 'STATUS', 'RESPONSE_SIZE', 'REFERRER', 'USERAGENT'], 'field list as expected with default log format');
	passed &= test_equal(regex.split('1.2.3.4 - - [31/Mar/2015:17:54:10 +0200] "GET / HTTP/1.1" 200 13 "-" "-"'), ['', '1.2.3.4', '-', '-', '31/Mar/2015:17:54:10 +0200', 'GET', '/', 'HTTP/1.1', '200', '13', '-', '-', ''], 'regex works on test data')

	exception1Thrown = False;
	test_format_fail = ' "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\"" combined';
	try:
		logFormat.parseFormatString(test_format_fail)
	except Exception as e:
		exception1Thrown = True;
	passed &= test_true(exception1Thrown, 'exeption thrown when logformat is invalid');

	return passed;
tests.append(test_logFormat);

print("""
--- pyLogParser test runner ---

used colors:
""")

log('warning', WARNING)
log('notice', NOTICE)
log('debug', DEBUG)

print();
print("found " + str(len(tests)) + ' test functions')
print();


for test in range(len(tests)):
	if not tests[test]():
		log('a testcase has failed!', WARNING);
		sys.exit();












