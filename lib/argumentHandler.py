import sys
import os

class UnknownArgumenError(Exception):
	pass;

ARGS = [
	{
		'type': 'flag',
		'short': '-h',
		'long': '--help',
		'conf_name': 'PRINT_HELP',
		'descr': 'print this help page'
	},
	{
		'type': 'file',
		'short': '-i',
		'long': '--input',
		'conf_name': 'INPUT',
		'descr': 'path to log file	--input "/path/to/file"'
	},
	{
		'type': 'file',
		'short': '-o',
		'long': '--output',
		'conf_name': 'OUTPUT',
		'descr': 'path to output file	--output "/path/to/file"'
	},
	{
		'type': 'file',
		'short': '-f',
		'long': '--format',
		'conf_name': 'FORMAT',
		'descr': 'path to format file	--input "/path/to/file"'
	}
];

class ArgumentHandler:
	def __init__(self, overrideArgv = None):
		if overrideArgv is None:
			self.__argv = sys.argv;
		else:
			self.__argv = overrideArgv;

		self.__args = {};
		self.__readArgv();


	def __readArgv(self):
		for i in range(len(self.__argv)):
			# skip script name in argv[0]
			if i == 0:
				continue;
			else:
				self.__parseArg(i);



	def __parseArg(self, argIndex):
		for i in range(len(ARGS)):
			if self.__argv[argIndex] in [ARGS[i]['short'], ARGS[i]['long']]:
				if ARGS[i]['type'] == 'flag':
					self.__args[ARGS[i]['conf_name']] = True;
					return 1;
				if ARGS[i]['type'] == 'file':
					try:
						self.__args[ARGS[i]['conf_name']] = self.__argv[argIndex + 1];
						return 2;
					except IndexError:
						return 1;



	def get(self, key):
		if key in self.__args:
			return self.__args[key];
		else:
			return False;

	def getHelp(self):
		helpStr = """
Usage: python pylogparser --format "/path/to/format.file" [OPTIONS]

this program generates JSON output from webserver logfiles, based
on the definitions in your format file. See decumentation for more
informations.

parameters:"""

		for arg in ARGS:
			helpStr += '\n' + arg['short'] +' | ' + arg['long'] + '	' + arg['descr'];

		return helpStr + '\n';
