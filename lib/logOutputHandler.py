import sys

LOG_SILENT = 0
LOG_CRITICAL = 1
LOG_WARNING = 2
LOG_NOTICE = 3
LOG_DEBUG = 4

LOG_STDERR = 100
LOG_SDTOUT = 101

LOGLEVEL_NAMES = {
	LOG_CRITICAL: "CRITICAL ERROR",
	LOG_WARNING: "WARNING",
	LOG_NOTICE: "NOTICE",
	LOG_DEBUG: "DEBUG INFO"
}


class LogOutputHandler:

	def __init__(self, logLevel = LOG_WARNING, destination = LOG_STDERR):
		self.logLevel = logLevel;
		self.out = False;
		if (destination == LOG_STDERR):
			self.out = sys.stderr;
		elif (destination == LOG_STDOUT):
			self.out = sys.stdout;
		else:
			try:
				self.out = open(destination, 'a')
			except Exception:
				print('Cannot open logfile for writing: ' + destination)

	def log(self, message, logLevel = LOG_WARNING):
		if ((logLevel > 0) and (logLevel <= self.logLevel)):
			print(LOGLEVEL_NAMES[logLevel] + ": " + message, file=self.out);

	def setLogLevel(self, logLevel):
		self.logLevel = logLevel;
