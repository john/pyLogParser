import geoip2.database

class GeoIP:
	def __init__(self, database_path):
		self.__geoip = None;
		try:
			self.__geoip = geoip2.database.Reader(database_path);
		except Exception as e:
			raise e;
		self.__cache = {};

	def getCountryByIp(self, ip):
		if ip in self.__cache:
			return self.__cache[ip];
		else:
			try:
				self.__cache[ip] = self.__geoip.country(ip).country.name;
			except Exception as e:
				self.__cache[ip] = 'Private IP';
			return self.__cache[ip];
