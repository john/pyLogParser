import re


class InvalidLogFormatError(Exception):
	pass

class LogFormatMapping:
	def getMappings(self):
		return [
			{
				'placeholder': "%h",
				'fields': ["CLIENT_IP"],
				'regex': "([(\d\.)]+)"
			},
			{
				'placeholder': "%l",
				'fields': ["CLIENT_IDENTIFIER"],
				'regex': "(.*?)"
			},
			{
				'placeholder': "%u",
				'fields': ["USERNAME"],
				'regex': "(.*?)"
			},
			{
				'placeholder': "%t",
				'fields': ["DATE"],
				'regex': "\[(.*?)\]"
			},
			{
				'placeholder': "%r",
				'fields': ["METHOD", "PATH", "HTTP_VERSION"],
				'regex': "(.*?) (.*?) (.*?)"
			},
			{
				'placeholder': "%>s",
				'fields': ["STATUS"],
				'regex': "(\d+)"
			},
			{
				'placeholder': "%b",
				'fields': ["RESPONSE_SIZE"],
				'regex': "(\d+)"
			},
			{
				'placeholder': "%{Referer}i",
				'fields': ["REFERRER"],
				'regex': "(.*?)"
			},
			{
				'placeholder': "%{User-agent}i",
				'fields': ["USERAGENT"],
				'regex': "(.*?)"
			}
		]

class LogFormat:

	def __init__(self):
		self.__mappings = LogFormatMapping();
		self.fieldList = [];
		self.regex = '';

	def stripFormatString(self, formatString):
		"""removes prefix and suffix from a log format string"""
		"""may throw a InvalidLogFormatError"""
		strip_regex = re.compile('^[a-zA-Z]+ "(.+)" [a-zA-Z]+$');
		match = strip_regex.match(formatString);
		if match and match .groups()[0]:
			return match.groups()[0];
		else:
			raise InvalidLogFormatError();

	def parseFormatString(self, formatString):
		"""returns the fieldlist and the regular expression for a given log format string"""
		"""may throw a InvalidLogFormatError"""
		try:
			self.regex = self.stripFormatString(formatString);
			self.fieldList = [];
			mapping = self.__mappings.getMappings();

			for known in range(len(mapping)):
				if not(self.regex.find(mapping[known]['placeholder']) == -1):
					self.fieldList += mapping[known]['fields'];
					self.regex = self.regex.replace(mapping[known]['placeholder'], mapping[known]['regex']);

			self.regex = re.compile(self.regex);

			if self.regex:
				return self.fieldList, self.regex;
			else:
				raise InvalidLogFormatError();

		except InvalidLogFormatError as e:
			raise e;

