import sys

class LineReader:
	def __init__(self, input_file = 'STDIN'):
		if input_file != 'STDIN':
			try:
				self.__input = open(input_file);
			except Exception as e:
				raise e;
		else:
			self.__input = sys.stdin;

	def readLines(self):
		for line in self.__input:
			yield line;
